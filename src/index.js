const isiOSnotSafari =
  navigator.userAgent.match("iPad") ||
  navigator.userAgent.match("iPhone") ||
  navigator.userAgent.match("iPod");

const isAndroid = navigator.userAgent.match("Android");
const criOs = navigator.userAgent.match("CriOS");
const IsChromeIos = criOs ? criOs[0] === "CriOS" : false;
const safari = navigator.userAgent.match(/Safari/i) != null;
const safariOnIos = !isAndroid && safari;

//user settings stored in localStorage
const makeMagic = () => {
  const bannerTemplate = `<div class="banner">
<p class="message">Open in app?</p>
<div class="button-wrapper">
  <button class="app-button button-yes">yes</button>
  <button class="app-button button-no">no</button>
</div>
<label class="ask-label">
  Don’t ask anymore'
  <input type="checkbox" class="ask" />
</label>
</div>`;
  //for test
  const buttonClear = document.querySelector(".clear");
  buttonClear.addEventListener("click", () => {
    localStorage.clear();
  });
  // end test
  let banner;
  let buttonYes;
  let buttonNo;

  const header = document.querySelector(".header");
  const isBannerShown = showBanner();

  if (isBannerShown) {
    addListeners();
  }

  function showBanner() {
    if (!isiOSnotSafari && safariOnIos) {
      return false;
    }
    if (!localStorage.getItem("navigate")) {
      //not safe
      header.innerHTML = bannerTemplate;
      return true;
    }

    const needNavigate = localStorage.getItem("navigate");
    if (needNavigate == "true") {
      navigate();
    }
    return false;
  }

  function hideBanner(needNavigate) {
    needShowAgain(needNavigate);
    banner.remove();
  }

  function needShowAgain(needNavigate) {
    const askCheckbox = document.querySelector(".ask");
    if (askCheckbox.checked) {
      localStorage.setItem("navigate", needNavigate);
    }
  }

  function addListeners() {
    banner = document.querySelector(".banner");
    buttonYes = document.querySelector(".button-yes");
    buttonNo = document.querySelector(".button-no");

    buttonNo.addEventListener("click", noButtonHandler);
    buttonYes.addEventListener("click", yesButtonHandler);
  }

  function noButtonHandler() {
    hideBanner(false);
  }

  function yesButtonHandler() {
    hideBanner(true);
    navigate();
  }

  function navigate() {
    // The links should be dynamic.
    if (isAndroid) {
      window.location.href =
        "intent://Configure?page=Accounts%2Fed0bfb5a-0e85-11ee-bff8-8da7252a85d8#Intent;scheme=sugarcrm;package=com.sugarcrm.nomad;end";
      return;
    }
    if (isiOSnotSafari) {
      window.location.href =
        "sugarcrm://Configure?page=Accounts%2Fed0bfb5a-0e85-11ee-bff8-8da7252a85d8";
      return;
    }
  }
};

addEventListener("DOMContentLoaded", makeMagic);
